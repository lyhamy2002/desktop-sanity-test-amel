import time
from utils.seleniumTools import find_element
from selenium.webdriver.support.ui import Select

class Logout(): # find the elements for logout
    def __init__ (self, d):
        self.driver = d
        self.drop_btn= 'xpath', '//*[@id="nav-menu"]' 
        self.logout_btn='xpath','//*[@id="celo-logout-button"]/div[1]'    
        # test case 18 assertion
        self.login_page = 'xpath', '//*[@id="app-content"]/app-root/div/div/app-home/div/div[1]/div[1]/div[1]/div'                         
                                    
    def logout(self): #encapsulated method for operation of web elements to logout
        find_element(self.driver, self.drop_btn).click()
        time.sleep(3)
        find_element(self.driver, self.logout_btn).click()