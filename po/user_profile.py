from utils.seleniumTools import find_element
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class UserProfile():
    def __init__(self, d):# elements of user profile
        self.driver = d
        self.user_name = 'xpath', '//*[@id="nav-menu"]/div[2]'
        self.view_profile = 'xpath', '//*[@id="mat-menu-panel-1"]/div/div/div[1]'
        #case 13 assertion
        self.view = 'xpath', '//*[@id="mat-menu-panel-1"]/div/div/div[1]/div[2]/div[2]'
        #case 14 assertion
        self.amy_sanitytest = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-profile-page/div/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]'
    
    def click_username(self):
        find_element(self.driver, self.user_name).click()
    
    def click_viewprofile(self):
        find_element(self.driver, self.view_profile).click()



    