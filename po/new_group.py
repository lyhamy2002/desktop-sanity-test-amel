from utils.seleniumTools import find_element
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class NewGroupCase(): 
    def __init__ (self, d): # elements for new group/case
        self.driver = d
        self.messagetab = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-conversations/div/div/div/div[1]/app-conversations-header/div[2]/div/a/app-create-new-conversation-button/div'
                                   #//*[@id="app-content"]/app-root/div/div[2]/app-conversations/div/div/div/div[1]/app-conversations-header/div[2]/div/a/app-create-new-conversation-button/div
        self.createnewmsg = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-directory/mat-sidenav-container/mat-sidenav-content/app-directory-company/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/div/div/div[3]/div[1]/div[1]/button'
                               #//*[@id="app-content"]/app-root/div/div[2]/app-conversations/div/div/div/div[1]/app-conversations-header/div[2]/div/a/app-create-new-conversation-button/div/div[2]
                               
        self.newgroup = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-directory/mat-sidenav-container/mat-sidenav-content/app-directory-company/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/div/div/div[3]/div[1]/div[1]/button'
        self.newcase = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-directory/mat-sidenav-container/mat-sidenav-content/app-directory-company/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/div/div/div[3]/div[1]/div[2]/button'
        self.workspacetab = 'xpath','//*[@id="mat-tab-label-8-0"]' 
        self.networktab = 'xpath', '//*[@id="mat-tab-label-8-1"]'
        
        self.searchspc = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-directory/mat-sidenav-container/mat-sidenav-content/app-directory-company/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/div/div/div[2]/div[1]/div/div/div/div[1]/app-celo-topbar-search-input/div/input'
        #amy eighteen                       
        self.user1 = 'xpath','//*[@id="mat-tab-content-9-0"]/div/div/div/div[1]/div[1]/app-grouped-contacts/div[1]/div/div/div[2]/div[1]/div/div[2]/app-celo-contact-card/app-celo-user-card/div'
        #amy liu
        self.user2 = 'xpath', '//*[@id="mat-tab-content-9-0"]/div/div/div/div[1]/div[1]/app-grouped-contacts/div[1]/div/div/div[2]/div[2]/div/div[2]/app-celo-contact-card/app-celo-user-card/div'
                            
        self.next = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-directory/mat-sidenav-container/mat-sidenav-content/app-directory-company/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/div/div/div[3]/div[2]/div/div/div[1]/button'
        
        self.groupname = 'xpath', '//*[@id="nameField"]' 
                                    
        self.creatgroup = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-directory/mat-sidenav-container/mat-sidenav-content/app-directory-company/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/div/div/div[2]/div[2]/app-new-message-create/form/div[2]/button[2]'
        self.cancelcreat = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-directory/mat-sidenav-container/mat-sidenav-content/app-directory-company/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/div/div/div[2]/div[2]/app-new-message-create/form/div[2]/button[1]'
        

    def newgroup(self): #method of creat new group
        find_element(self.driver, self.messagetab).click()
        find_element(self.driver, self.createnewmsg).click()
        find_element(self.driver, self.newgroup).click()
        find_element(self.driver, self.searchspc).send_keys('amy')
        find_element(self.driver, self.searchspc).send_keys(Keys.ENTER)
        find_element(self.driver, self.user1).click()
        find_element(self.driver, self.user2).click()
        find_element(self.driver, self.next).click()
        find_element(self.driver, self.groupname).click()
        find_element(self.driver, self.groupname).send_keys('auto group')
        find_element(self.driver, self.creatgroup).click()
    
   
        
