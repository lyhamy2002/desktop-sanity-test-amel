from utils.seleniumTools import find_element

class CeloLogin():
    def __init__ (self, d): #elements of login email, pwd & cfm btn
        self.driver = d
        self.mailaddr= 'id','Username' #find mailaddress text box
        self.pwd='id','Password'      #find pwd text box
        self.login_btn = 'xpath','/html/body/div/div/div[1]/div/div/div[3]/div/form/fieldset/div[3]/button' #find login btn
            
    def login(self, email, password):
        find_element(self.driver, self.mailaddr).send_keys(email)
        find_element(self.driver, self.pwd).send_keys(password)
        find_element(self.driver, self.login_btn).click()
        


