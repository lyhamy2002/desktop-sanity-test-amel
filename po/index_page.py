from utils.seleniumTools import find_element

class IndexPage():
    def __init__ (self, d): #elements of login & signup
        self.driver = d
        self.login = 'xpath', '//*[@id="login"]'
        self.signup = 'xpath', '//*[@id="signup"]'
        self.login_word = 'xpath', '//*[@id="app-content"]/app-root/div/div/app-home/div/div[1]/div[1]/div[1]/div'
            
    def login_click(self):
        find_element(self.driver, self.login).click()
    
    def signup_click(self):
        find_element(self.driver, self.signup).click()

