from utils.seleniumTools import find_element
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.select import Select
import time

class SettingPage(): 
    def __init__(self, d): #elements of general, security from drop down menu
        self.driver = d
        self.user_amysanity = 'xpath', '//*[@id="nav-menu"]/div[2]' #//*[@id="nav-menu"]/div[2]
        self.general_tab = 'xpath', '//*[@id="mat-menu-panel-1"]/div/div/button[3]'  #//*[@id="mat-menu-panel-1"]/div/div/button[3]/a/div
                                     
        #security tab on Setting page                              
        self.security_tab = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-edit/mat-sidenav-container/mat-sidenav-content/div/div[1]/ul/li[3]'
        #                              
        #case 15 assertion           
        self.general_text = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-edit/mat-sidenav-container/mat-sidenav-content/div/div[2]/div/app-setting/div[2]/div/div/div/div[1]/div[1]'
        #case 16 assertion            
        self.security_text = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-edit/mat-sidenav-container/mat-sidenav-content/div/div[2]/div/app-setting/div[1]'   
                                      
    def general(self):
        find_element(self.driver, self.user_amysanity).click()
        time.sleep(3)
        find_element(self.driver, self.general_tab).click()

    
    def security(self):
        find_element(self.driver, self.security_tab).click()
   

    
        
