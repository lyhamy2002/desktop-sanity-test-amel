from utils.seleniumTools import find_element
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class MyNetWork():
    def __init__(self, d): #elements of Network
        self.driver = d
        self.network_tab = 'xpath','//*[@id="nav-directory"]'   
        self.smlnetwork_tab = 'xpath', '//*[@id="mat-tab-label-0-0"]'           
        self.smlworkspace_tab = 'xpath', '//*[@id="mat-tab-label-0-1"]'  
        self.workspace_list = 'xpath', '//*[@id="mat-tab-content-0-1"]/div/div/div[1]'
        self.workspace_search = 'xpath', '//*[@id="mat-input-3"]' 
        self.workspace_celo = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-directory/mat-sidenav-container/mat-sidenav-content/app-directory-company/div/mat-sidenav-container/mat-sidenav/div/div/div[3]/ul/li[1]' 
        #celo_dept is for assert
        self.celo_name = 'xpath', '//*[@id="mat-tab-content-0-1"]/div/div/div[1]/div[2]/div/div[1]'
                                   
    def celo_mynetwork(self): #case 05
        find_element(self.driver, self.network_tab).click()

    def celo_network(self):   #case 06
        find_element(self.driver, self.network_tab).click()

    def celo_workspace(self): #case 07
        find_element(self.driver, self.smlworkspace_tab).click()
    
    def switch_to_celo(self): #case 08:switch WS to be Celo
        find_element(self.driver, self.network_tab).click()
        find_element(self.driver, self.smlworkspace_tab).click()
        find_element(self.driver, self.workspace_list).click()
        find_element(self.driver, self.workspace_search).click()
        find_element(self.driver, self.workspace_search).clear()
        find_element(self.driver, self.workspace_search).send_keys('celo')
        find_element(self.driver, self.workspace_search).send_keys(Keys.ENTER)
        find_element(self.driver, self.workspace_celo).click()




