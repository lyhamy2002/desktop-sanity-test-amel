from utils.seleniumTools import find_element
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class SecureLibrary():
    def __init__(self, d): #elements of check secure lib, media, consents & doc
        self.driver = d
        self.securelib_tab = 'xpath', '//*[@id="nav-secure-library"]'
        self.media = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-library/div[1]/app-celo-side-menu-sub-list/ul/div/li[1]'
        self.consents = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-library/div[1]/app-celo-side-menu-sub-list/ul/div/li[2]'
        self.docu = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-library/div[1]/app-celo-side-menu-sub-list/ul/div/li[3]'
        #case 09 assertion
        self.lib_name = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-library/div[1]/div/div'
        #case 10 assertion
        self.select_media = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-library/div[2]/app-media/div/div/div[2]/div[4]/app-library-secondbar-toolbar/div/div/button'
        #case 11 assertion
        self.select_consents = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-library/div[2]/app-consents/div/div/div[1]/div[3]/div[1]/app-library-secondbar-toolbar/div/div/button'
        #case 12 assertion
        self.select_doc = 'xpath', '//*[@id="app-content"]/app-root/div/div[2]/app-library/div[2]/app-documents/div/div/div[2]/div[3]/app-library-secondbar-toolbar/div/div/button'
    
    def click_securelib(self):
        find_element(self.driver, self.securelib_tab).click()
    
    def click_media(self):
        find_element(self.driver, self.media).click()
    
    def click_consents(self):
        find_element(self.driver, self.consents).click()
    
    def click_docu(self):
        find_element(self.driver, self.docu).click()



    

