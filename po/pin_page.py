from utils.seleniumTools import find_element

class SetPIN(): 
    def __init__ (self, d):# elements of PIN page
        self.driver = d
        self.pin= 'xpath', '//*[@id="pin_code"]' 
                            
        self.cfm_pin='xpath','//*[@id="pin_code_confirm"]'      
        self.next_btn = 'xpath','//*[@id="app-content"]/app-root/div/div/app-pinscreen/div/div/div/div[3]/button' 
                             
    def send_pin(self, pin, cfm_pin): #send PIN, confirm
        find_element(self.driver, self.pin).send_keys(pin)
        find_element(self.driver, self.cfm_pin).send_keys(cfm_pin)
        find_element(self.driver, self.next_btn).click()