from utils.seleniumTools import find_element
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class SendMessage(): 
    def __init__ (self, d): # elements of search someone, then send msg
        self.driver = d
        self.message_tab = 'xpath', '//*[@id="nav-conversations"]'
        self.search_box = 'xpath', '//*[@id="mat-input-2"]' 
        self.username = 'xpath','//*[@id="conversations-scroll"]/div[2]/app-conversation-results/div/div[1]/a[1]/app-conversation-card/div/div[1]/div[2]'
        self.msg_textarea = 'xpath', '//*[@id="celo-send-message-textarea"]'
        self.send_btn = 'xpath','/html/body/app-root/div/div[2]/app-conversations/div/app-messages/div/div[3]/mat-sidenav-container/mat-sidenav-content/div/div[2]/div/app-messages-bottom-text-bar/div/button' 
                                 
    def search_id_msg(self): #find 'Amy Liu' and send msg
        find_element(self.driver, self.message_tab).click()
        find_element(self.driver, self.search_box).click()
        find_element(self.driver, self.search_box).send_keys('amy liu')
        find_element(self.driver, self.search_box).send_keys(Keys.ENTER)
        find_element(self.driver, self.username).click()
        find_element(self.driver, self.msg_textarea).click()
        find_element(self.driver, self.msg_textarea).send_keys('test')
        find_element(self.driver, self.msg_textarea).send_keys(Keys.ENTER)


    # def send_message(self, message): 
    #     # find_element(self.driver, self.admin_qa).click()
    #     find_element(self.driver, self.msg_box).send_keys(message)
    #     find_element(self.driver, self.send_btn).click()