import unittest
from utils.HTMLTestRunner import HTMLTestRunner

# run & generate report
title = "Celo Web Sanity Test"
descr = "-- by Amy Liu"
file_path = "report/report (Celo-Amy Liu).html"
# collect test case
t = unittest.defaultTestLoader.discover("case", "test_*.py")

with open(file_path, "wb") as f:
    runner = HTMLTestRunner(stream=f, title=title, description=descr)
    runner.run(t)