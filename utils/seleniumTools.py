
from selenium.webdriver.support.ui import WebDriverWait

# encapsulated function for find webpage elements via WebDriverWait
def find_element(driver, locator, timeout=20):
    '''
    locator = 'method', 'value'
    '''
    return WebDriverWait(driver, timeout).until(lambda s: s.find_element(*locator))

# encapsulated function for assert element exist 
def assert_element_exist(driver, locator, timeout=20):
    try:
        find_element(driver, locator, timeout)
        return True
    except:
        return False