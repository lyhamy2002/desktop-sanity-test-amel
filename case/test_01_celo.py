import unittest
import time
import random
import requests
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from po.index_page import IndexPage
from po.login_page import CeloLogin
from po.pin_page import SetPIN
from po.msg_page import SendMessage
from po.logout import Logout
from po.my_network import MyNetWork
from po.library import SecureLibrary
from po.user_profile import UserProfile
from po.setting_page import SettingPage
from po.new_group import NewGroupCase
from selenium.webdriver.chrome.options import Options
from utils.seleniumTools import find_element
from utils.seleniumTools import assert_element_exist


class CeloWeb(unittest.TestCase):

    
    @classmethod
    def setUpClass(cls):
        #disable notification in Chrome
        option = Options()
        option.add_argument('--disable-infobars')
        option.add_argument('start-maximized')
        option.add_argument('--disable-extensions')
        # 1 to allow & 2 to block
        option.add_experimental_option("prefs", {
            'profile.default_content_setting_values.notifications': 1
        })
        

        # launch chrome & open Celo Web prod web
        cls.driver = webdriver.Chrome(chrome_options = option)
        cls.driver.maximize_window()
        cls.driver.get('https://app.celohealth.com/')
        cls.driver.implicitly_wait(5)

    # click login btn

    def test_01_index(self):
        index = IndexPage(self.driver)

        #assert index page loaded 
        expect_result = index.login
        assert assert_element_exist(self.driver, expect_result) == True  

        # click login in index page
        index.login_click()

    # send username, password and click login
    def test_02_login(self):
        log = CeloLogin(self.driver)

        expect_result = log.login_btn
        assert assert_element_exist(self.driver, expect_result) == True
        
        # send login info and click next
        log.login("amy.liu+sanitest@celohealth.com", "Celo098&")

        # send PIN
    def test_03_pin(self):
        pin = SetPIN(self.driver)

        expect_result = pin.next_btn
        assert assert_element_exist(self.driver, expect_result) == True

        #send pin
        pin.send_pin("2580", "2580")
    
    # send message 
    def test_04_sendmsg(self):
        send_msg = SendMessage(self.driver)

        expect_result = send_msg.message_tab
        assert assert_element_exist(self.driver, expect_result) == True

        send_msg.search_id_msg() #search 'amy liu", click and send message 'test'

    def test_05_mynetwork(self):# click 'My Network' tab
        my_network = MyNetWork(self.driver)
        my_network.celo_mynetwork()
        
        #assert "network" tab shows after click "My Network"
        expect_result = my_network.smlnetwork_tab
        assert assert_element_exist(self.driver, expect_result) == True
    
    def test_06_network(self): # click 'NETWORK' under 'My Network'
        network = MyNetWork(self.driver)
        network.celo_network()
        self.driver.implicitly_wait(3)

        #assert the text is 'Connection' below the NETWORK tab 
        connection_word = self.driver.find_element_by_xpath('//*[@id="mat-tab-content-0-0"]/div/div/div/div[1]/div[1]/app-grouped-contacts/div[1]/div/div/div[1]/div')
        assert connection_word.text == 'Connections'
    
     
    def test_07_workspace(self): #click 'WORKSPACE' under 'My Network'.   got assertion erron 11-Jan-20
        workspace = MyNetWork(self.driver)
        workspace.celo_workspace()

        #assert the text 'Click here to switch directory' below the WORKPLACE tab doescreatenewmsg     expect_result = workspace.workspace_list
        expect_result = workspace.celo_name
        assert assert_element_exist(self.driver, expect_result) == True
    
    def test_08_switchwp(self): #switch to company == 'Celo'
        switchwp = MyNetWork(self.driver)
        switchwp.switch_to_celo()
        #time.sleep(3)

        #assert the company name is 'Celo'
        expect_result = switchwp.celo_name
        assert assert_element_exist(self.driver, expect_result)

    def test_09_securelib(self): #click 'Secure Library' tab
        celo_lib = SecureLibrary(self.driver)
        celo_lib.click_securelib()

        #assert the test at up-left corner is 'Secure Li/brary', iframe
        expect_result = celo_lib.lib_name
        assert assert_element_exist(self.driver, expect_result) == True
  
    def test_10_media(self): #click 'Media' side tab inside 'Secure Library'
        celo_media = SecureLibrary(self.driver)
        celo_media.click_media()

        #assert 'Select photos' does exist
        expect_result = celo_media.select_media
        assert assert_element_exist(self.driver, expect_result) == True

    def test_11_consents(self): #click 'Consents' side tab inside 'Secure library'
        celo_consents = SecureLibrary(self.driver)
        celo_consents.click_consents()
        
        #assert 'Select consents' exist
        expect_result = celo_consents.select_consents
        assert assert_element_exist(self.driver, expect_result) == True

    def test_12_docu(self): #click "Documents" side tab inside 'Secure Library'
        celo_doc = SecureLibrary(self.driver)
        celo_doc.click_docu()

        #assert 'Select documents' exist
        expect_result = celo_doc.select_doc
        assert assert_element_exist(self.driver, expect_result) == True

    def test_13_dropdown(self):#click user name at up-right corner
        user_name = UserProfile(self.driver)
        user_name.click_username()

        #assert there is 'View your profile' under user name
        expect_result = user_name.view
        assert assert_element_exist(self.driver, expect_result) == True
    
    def test_14_profile(self): #click view your profile
        view_profile = UserProfile(self.driver)
        view_profile.click_viewprofile()

        #assert goes to the profile page
        expect_result = view_profile.amy_sanitytest
        assert assert_element_exist(self.driver, expect_result) == True
    
    def test_15_general(self): #click username at up-right corner, then click 'Privacy' tab 
        setting_page = SettingPage(self.driver)
        setting_page.general()

        #assert 'Privacy' page show
        expect_result = setting_page.general_text
        assert assert_element_exist(self.driver, expect_result) == True
    
    def test_16_security(self): # click 'Security' tab
        setting_page = SettingPage(self.driver)
        setting_page.security()
        
        #assert "Security" page shows
        expect_result = setting_page.security_text
        assert assert_element_exist(self.driver, expect_result) == True
    
    def test_17_newgroup(self): #creat a new group
        new_group = NewGroupCase(self.driver)
        new_group.newgroup()

        #assert new group named "Auto group" exist
        expect_result = self.driver.find_element_by_xpath('//*[@id="conversations-scroll"]/div[2]/app-conversation-list/div/div[1]/a/app-conversation-card/div/div[1]/div[2]')
                                                          
        assert assert_element_exist(self.driver, expect_result) == True

    def test_18_logout(self): # logout and go to index page, test passed but put it to the end of plan
        drop_down = Logout(self.driver)
        drop_down.logout()

        # assert goes to the index page
        expect_result = drop_down.login_page
        assert assert_element_exist(self.driver, expect_result) == True 

    
    #close chrome
    @classmethod  
    def tearDownClass(cls):
        cls.driver.quit()


if __name__ == '__main__':
    unittest.main()
